#Sample program to demonstrate bit stuffing submitted as part of assognment 2 in Advanced topics in networks
#Developed by Asok Kalidass K - B00763356
import binascii
#read the given file
bit_stuffing = []
original_content = []
count = 0
def TextToBitStream( ):
    file = open('BitStuffing_input.txt', 'r')
    data = file.read()
    binary_Content = bin(int(binascii.hexlify(data), 16))
    #Declarations
    global bit_stuffing
    global original_content
    global count
    #Hexify the data ( string to binary conversion)
    for i in binary_Content:    

        if count == 5:
            bit_stuffing.append(0)
            count = 0

        if i == '1':
            count = count + 1 
            bit_stuffing.append(i)

        if i == '0':
            count = 0
            bit_stuffing.append(i)
       
    print(len(bit_stuffing))
    count = 0

#Binary to hex conversion
def HexttoText():
    global count
    global original_content
    for i in bit_stuffing:    
        ignore = False    
        if count == 5:
            #bit_stuffing.append(0)
            count = 0
            ignore = True

        if i == '1':
            count = count + 1 
            original_content.append(i)

        if i == '0' and ignore == False:
            count = 0
            original_content.append(i)

    print(len(original_content))

    print(len(set(bit_stuffing) & set(original_content)))
    new_content = "0b"
    for i in original_content:
        new_content += i

    print(new_content)
    #unhexify the data
    unhexified_data = int(new_content, 2)
    #original content is retained
    original_content = binascii.unhexlify('%x' % unhexified_data)

    print(original_content)
    #Writing the content to the output file. 
    with open("Output.txt", "w") as op:
        op.write(str(original_content))

#Main function to perform the bit stuffing operations
def main():

    TextToBitStream()
    HexttoText()

#Program Execution point
if __name__== "__main__":
    main()








